<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Book
 * Контроллер для работы с книгами
 */
class Book extends CI_Controller {

    /**
     * Инициализируем модель
     */
    public function __construct()
    {
        parent::__construct();

        $this->load->model('Book_model');
    }

    /**
	 * Загрузка списка книг
	 */
	public function loadList()
	{
		$bookList = $this->Book_model->loadList();
		echo json_encode($bookList);
	}

    /**
     * Редактирование элемента
     */
    public function editItem()
    {
        $id = $this->input->post('book_id');

        $author_name = $this->input->post('author_name');
        $book_name = $this->input->post('book_name');
        $book_year = $this->input->post('book_year');

        $data =
        [
            'author_name'=>$author_name,
            'book_name'=>$book_name,
            'book_year'=>$book_year,
        ];

        $this->Book_model->update($id,$data);
    }

    /**
     * Добавление элемента
     */
    public function addItem()
    {

        $author_name = $this->input->post('author_name');
        $book_name = $this->input->post('book_name');
        $book_year = $this->input->post('book_year');

        $data =
            [
                'author_name'=>$author_name,
                'book_name'=>$book_name,
                'book_year'=>$book_year,
            ];

        $this->Book_model->add($data);
    }

    /**
     * Удаление элемента
     */
    public function deleteItem(){

        $id = $this->input->post('book_id');

        $this->Book_model->delete($id);

    }

    public function importXML(){

        $this->load->helper('file');

        $bookList = $this->Book_model->loadList();

        if ($bookList) {

            $books = new SimpleXMLElement('<?xml version="1.0" encoding="utf-8"?><books></books>');

            foreach($bookList as $item) {

                $book = $books->addChild('book');
                $book->addAttribute('id', $item['book_id']);
                $book->addAttribute('name', $item['book_name']);
                $book->addAttribute('author', $item['author_name']);
            }
        }

        $path = '/books.xml';

        $result_array = [];

        if ( ! write_file($path, $books->asXML()))
        {

            $result_array["result"] = 'error';
            $result_array["path"] = '';

        }
        else
        {

            $result_array["result"] = 'success';
            $result_array["path"] = $path;

        }

        echo json_encode($result_array);
    }

}
