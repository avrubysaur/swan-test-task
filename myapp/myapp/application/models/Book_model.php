<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * Class Book_model
 * Модель для работы с книгами
 */
class Book_model extends CI_Model {


    public function __construct()
    {
        $this->load->database();
    }

	/**
	 * Загрузка списка книг
	 */
	public function loadList()
	{
        $query = $this->db->get('book');

        return $query->result_array();
	}

	public function update($id,$data){

        $this->db->where('book_id', $id);
        $this->db->update('book', $data);

    }

    public function add($data){

        $this->db->insert('book', $data);

    }

    public function delete($id){

        $this->db->where('book_id', $id);
        $this->db->delete('book');

    }

}
