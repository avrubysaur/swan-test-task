/**
 * Список книг
 */

Ext.define('MyViewController', {
    extend : 'Ext.app.ViewController',
    alias: 'controller.book',

    onAddClick: function() {
        Ext.widget('addwindow');
    },

    setNoSelectMsg: function(){

        Ext.Msg.alert('Ошибка', 'Не выбрано ни одного элемента');

    },

    onEditClick: function() {


            var grid = this.getView();

            var selectionModel = grid.getSelectionModel();

            if (selectionModel.hasSelection()) {

                var record = selectionModel.getSelected().items[0];

                var view = Ext.widget('editwindow');

                var form = view.down('form');

                form.loadRecord(record);

            }

            else{

                this.setNoSelectMsg();

            }

    },

    onDeleteClick: function () {

        var grid = this.getView();

        var selectionModel = grid.getSelectionModel();

        if (selectionModel.hasSelection()) {

            var bookName = selectionModel.getSelected().items[0].data.book_name;

            var confirm = Ext.Msg.confirm(bookName, "Вы действительно хотите удалить элемент?", function (btnText) {

                if (btnText === "no") {

                    confirm.close();

                } else if (btnText === "yes") {

                    Ext.Ajax.request({
                        url: 'index.php/Book/deleteItem',
                        method: "post",

                        params: {
                            book_id: selectionModel.getSelected().items[0].data.book_id,
                        },
                        success: function (response) {

                            grid.getStore().reload();

                        }
                    });
                }
            });

        } else {

            this.setNoSelectMsg();

        }

    },
    onExportClick: function () {

        Ext.Ajax.request({
            url: 'index.php/Book/importXML',
            method: "post",
            success: function (response) {
                //console.log(response.responseText);
                var json = JSON.parse(response.responseText);

                if(json['result'] === 'success'){

                    window.open(json.path);

                }
                else{

                    Ext.Msg.alert('Ошибка', 'Не удалось выгрузить данные');

                }

            }
        });

    }


});

Ext.define('Swan.view.Books', {
    extend: 'Ext.grid.Panel',
    id: 'book-container',
    controller: 'book',
    referenceHolder: true,
    reference: 'b-cont',
    store: {
        proxy: {
            type: 'ajax',
            url: 'index.php/Book/loadList',
            reader: {
                type: 'json',
                idProperty: 'book_id'
            }
        },
        autoLoad: true,
        remoteSort: false,
        sorters: [{
            property: 'book_name',
            direction: 'ASC'
        }]
    },
    tbar: [{
        text: 'Добавить',
        handler: 'onAddClick',
    }, {
        text: 'Редактировать',
        id: 'edit-button',
        handler: 'onEditClick',
    }, {
        text: 'Удалить',
        handler: 'onDeleteClick',
    }, {
        text: 'Экспорт в XML',
        handler: 'onExportClick',
    }],
    columns: [{
        dataIndex: 'author_name',
        text: 'Автор',
        width: 150,
        editable: 'true',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        }
    }, {
        dataIndex: 'book_name',
        text: 'Название книги',
        flex: 1,
        editable: 'true',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        }
    }, {
        dataIndex: 'book_year',
        text: 'Год издания',
        width: 150,
        editable: 'true',
        editor: {
            xtype: 'textfield',
            allowBlank: false
        }
    }],
    selectable: {
        columns: false, // Can select cells and rows, but not columns
        extensible: true // Uses the draggable selection extender
    },
});
