Ext.define('Swan.view.Book', {
    extend: 'Ext.window.Window',
    alias: 'widget.editwindow',

    title: 'Книга',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [{
            xtype: 'form',
            items: [
            {
                    xtype : 'hidden',  //should use the more standard hiddenfield
                    name  : 'book_id'
            },
            {
                xtype: 'textfield',
                name : 'book_name',
                fieldLabel: 'Название'
            },{
                xtype: 'textfield',
                name : 'author_name',
                fieldLabel: 'Автор'
            },{
                xtype: 'numberfield',
                name : 'book_year',
                fieldLabel: 'Год',
                minValue: 1,
            }]
        }];
        this.buttons = [
        {
                text: 'Сохранить',
                scope: this,
                handler: editHandler,
        },
        ];

        this.callParent(arguments);
    },


});

var editHandler = function(button,event) {
    var form = this.down('form');
    if(form.isValid()){

        form.submit({
            url:'index.php/Book/editItem',
            waitMsg:'Идёт сохранение данных...',
            success:function () {

                var book_container = Ext.ComponentQuery.query('#book-container')[0];
                book_container.getStore().reload();

            }
        });

    }
    else{

        Ext.MessageBox.alert('Errors', 'Ошибка. Необходимые поля не заполнены');

    }

};

Ext.define('Swan.view.Book', {
    extend: 'Ext.window.Window',
    alias: 'widget.addwindow',

    title: 'Книга',
    layout: 'fit',
    autoShow: true,

    initComponent: function() {
        this.items = [{
            xtype: 'form',
            items: [
                {
                    xtype: 'textfield',
                    name : 'book_name',
                    fieldLabel: 'Название'
                },{
                    xtype: 'textfield',
                    name : 'author_name',
                    fieldLabel: 'Автор'
                },{
                    xtype: 'numberfield',
                    name : 'book_year',
                    fieldLabel: 'Год',
                    minValue: 1,
                }]
        }];
        this.buttons = [
            {
                text: 'Сохранить',
                scope: this,
                handler: addHandler,
            },
        ];

        this.callParent(arguments);
    },


});

var addHandler = function(button,event) {
    var form = this.down('form');
    if(form.isValid()){

        form.submit({
            url:'index.php/Book/addItem',
            waitMsg:'Идёт сохранение данных...',
            success:function () {

                var book_container = Ext.ComponentQuery.query('#book-container')[0];
                book_container.getStore().reload();

            }
        });

    }
    else{

        Ext.MessageBox.alert('Errors', 'Ошибка. Необходимые поля не заполнены');

    }

};
